defaults
==========

Ansible collection of defaults scripts. 

Includes:

- motd: replaces and backups the /etc/motd file on Debian 9
- packages: installs some default packages
- users: creates `ansible` & `selbstmade` users
- ssh: secures ssh access
- system_security: installs some security packages


Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`defaults_motd_file` | Path to motd file (default `files/motd`)
`defaults_packages` | List of packages to be installed `aptitude, curl, cron, htop, nano, etc ...`
`defaults_public_keys` | List of public keys for the selbstmade user
`defaults_ssh_sshd_config` | Path to ssh config (default `/etc/ssh/sshd_config`)
`defaults_ssh_permit_empty_password` | Allow empty root pwds (default `no`)
`defaults_ssh_permit_root_login` | Allow root login (default `false`)
`defaults_ssh_sshd_disable_password_login` | Disable password login (default `false`)
`defaults_ssh_use_pam` | Use PAM (default `true`)
`defaults_ssh_port` | SSH port (default `22`)
`defaults_system_security_packages` | List of packages to install for security `fail2ban, logwatch, etc ...`
`defaults_system_security_logwatch_email` | E-Mail address for logwatch emails (default: `security@selbstmade.ch`)




Example Playbook
----------------

```yaml
# file: test.yml
- hosts: local

  vars:
    defaults_motd_file: /path/to/my/motd
    defaults_packages: 
    - package1
    - package2
    defaults_ssh_use_pam: false

  roles:
    - defaults
```
